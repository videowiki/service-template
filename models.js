const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const MODEL_NAME = '';


const ModelSchema = new Schema({
  
  created_at: { type: Date, default: Date.now, index: true },
  updated_at: { type: Date, default: Date.now },
})

ModelSchema.pre('save', function (next) {
  const now = new Date()
  this.updated_at = now
  if (!this.created_at) {
    this.created_at = now
  }
  return next();
})

ModelSchema.statics.isObjectId = (id) =>
  mongoose.Types.ObjectId.isValid(id)

  ModelSchema.statics.getObjectId = (id) =>
  mongoose.Types.ObjectId(id)


const Model = mongoose.model(MODEL_NAME, ModelSchema)

module.exports = { Model };
