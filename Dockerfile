FROM node:10.21.0-alpine3.11
WORKDIR /SERVICE_NAME-service

COPY . .
RUN npm install

EXPOSE 4000
CMD [ "npm", "run", "docker:prod"]